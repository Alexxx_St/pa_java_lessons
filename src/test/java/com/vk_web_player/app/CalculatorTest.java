package com.vk_web_player.app;

import org.junit.Test;

import static org.junit.Assert.*;

public class CalculatorTest {

    @Test
    public void calc() {
        Calculator calculator = new Calculator();
        calculator.calc(10,1,"+");
        assertEquals(11,calculator.result);
    }
}