package com.vk_web_player.app;
public class Calculator {

    public int result = 0;
    //public int tmp = 0;


    public void calc(int first, int second, String operation){
        if ("+".equals(operation)) {
            this.result = first + second;
        } else if ("-".equals(operation)) {
            this.result = first - second;
        } else if ("*".equals(operation)) {
            this.result = first * second;
        } else if ("/".equals(operation)) {
            this.result = first / second;
        } else {
            this.result = 0;
        }

    }
}