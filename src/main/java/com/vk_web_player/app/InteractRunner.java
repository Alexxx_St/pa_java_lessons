package com.vk_web_player.app;

import java.util.Scanner;

public class InteractRunner{
    public static void main ( String[] arg ) {
        Scanner reader = new Scanner(System.in);
        try {
            String exit = "no";
            String clearResult = "yes";
            Calculator calk = new Calculator();
            String first;
            String second;
            String operation;


            while (!exit.equals("yes")){
                if (clearResult.equals("no")){
                    System.out.println("first number is " + calk.result);
                    first = Integer.toString(calk.result);
                } else {
                    System.out.println("enter first number...");
                    first = reader.next();
                }
                System.out.println("enter second number...");
                second = reader.next();
                System.out.println("enter operation type...");
                System.out.println("| + | - | * | / |");
                operation = reader.next();
                calk.calc(Integer.valueOf(first), Integer.valueOf(second), operation);
                System.out.println("processing...");
                System.out.println("result is: " + calk.result);
                System.out.println("Clear result? yes/no");
                clearResult = reader.next();
                System.out.println("exit:? yes/no");
                exit = reader.next();



            }
        } catch (Exception e){
            System.out.println("wrong type! try again");
        } finally {
            reader.close();
        }

    }
}